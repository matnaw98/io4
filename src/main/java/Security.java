import edu.ib.springdataexample.service.PasswordEncoderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

public class Security {
    @EnableWebSecurity
    @Configuration
    public static class SecurityConfig extends WebSecurityConfigurerAdapter {
        @Autowired
        PasswordEncoderConfig passwordEncoderConfig;

        @Autowired
        DataSource dataSource;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.jdbcAuthentication()
                    .usersByUsernameQuery("SELECT u.name, u.pass, 1 FROM user u WHERE u.name=?")
                    .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM user u WHERE u.name=?")
                    .dataSource(dataSource)
                    .passwordEncoder(passwordEncoderConfig.passwordEncoder());
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .formLogin().permitAll()
                    .and()
                    .logout().permitAll()
                    .and()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.GET,"/api/customer/all").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/customer/one").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/order/all").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/order/one").permitAll()
                    .antMatchers(HttpMethod.POST,"/api/order").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/product/all").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/product/one").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/product/one").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/user/all").hasAnyRole("CUSTOMER","ADMIN")
                    .antMatchers(HttpMethod.GET,"/api/user").hasAnyRole("CUSTOMER","ADMIN")
                    .antMatchers(HttpMethod.POST,"/api/product/admin/product").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST,"/api/customer/admin/customer").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PUT,"/api/product/admin/product").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PUT,"/api/customer/admin/customer").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PUT,"/api/order/admin/order").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PATCH,"/api/product/admin/product").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PATCH,"/api/customer/admin/customer").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PATCH,"/api/order/admin/order").hasRole("ADMIN");
        }
    }
}
