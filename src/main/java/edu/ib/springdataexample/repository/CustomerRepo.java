package edu.ib.springdataexample.repository;

import edu.ib.springdataexample.controler.Customer;
import edu.ib.springdataexample.controler.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
public interface CustomerRepo extends CrudRepository<Customer, Long> {

}
