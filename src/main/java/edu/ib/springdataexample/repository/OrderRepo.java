package edu.ib.springdataexample.repository;


import edu.ib.springdataexample.controler.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepo extends CrudRepository<Order,Long> {
}

