package edu.ib.springdataexample.repository;


import edu.ib.springdataexample.controler.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends CrudRepository<Product,Long> {
}
