package edu.ib.springdataexample.repository;

import edu.ib.springdataexample.controler.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface UserRepo extends CrudRepository<UserDto,Long> {
}
