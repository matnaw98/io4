package edu.ib.springdataexample.service.manager;


import edu.ib.springdataexample.controler.User;
import edu.ib.springdataexample.controler.UserDto;
import edu.ib.springdataexample.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserDtoBuilder {
    private UserRepo userRepo;

@Autowired
    public UserDtoBuilder(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
    User user = new User("jan","$2a$16$AYSTH/JEPXPwqYBRswLq0emwmItvsQgf.dnaffXGJpvYML97bBoGe","CUSTOMER");
    User user2 = new User("ala","kot","ADMIN");
        User user3 = new User("konrad","44","CUSTOMER");
        User user4 = new User("adam","mickiewicz","ADMIN");

        UserDto userDto = new UserDto(user.getName(),user.getPassword(),user.getRole());
        UserDto adminDto = new UserDto(user2.getName(),user2.getPassword(),user2.getRole());
        UserDto userDto2 = new UserDto(user3.getName(),user3.getPassword(),user3.getRole());
        UserDto adminDto2 = new UserDto(user4.getName(),user4.getPassword(),user4.getRole());
        userRepo.save(userDto);
        userRepo.save(adminDto);
        userRepo.save(userDto2);
        userRepo.save(adminDto2);
    }
    @GetMapping("/all")
    public List<UserDto> findAll() {
        return (List<UserDto>) userRepo.findAll();
    }
    @GetMapping()
    public Optional<UserDto> findById(@RequestParam Long id) {
        return userRepo.findById(id);
    }



}
