package edu.ib.springdataexample.service.manager;

import edu.ib.springdataexample.controler.Customer;
import edu.ib.springdataexample.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/customer")
public class CustomerManager {

    private CustomerRepo customerRepo;

    @Autowired
    public CustomerManager(CustomerRepo customerRepo) {
        this.customerRepo= customerRepo;
    }

    @GetMapping("/all")
    public List<Customer> findAll() {
        return (List<Customer>) customerRepo.findAll();
    }

    @GetMapping("/one")
 public Optional<Customer> findById(@RequestParam Long id){
        return customerRepo.findById(id);
    }

    @PostMapping("/admin/customer")
    public Customer add(@RequestBody Customer customer) {
        return customerRepo.save(customer);
    }

    @PutMapping("/admin/customer")
    public Customer update(@RequestBody Customer customer, @RequestParam Long id) {
        customerRepo.deleteById(id);
        customer.setId(id);
        return customerRepo.save(customer);
    }

    @PatchMapping("/admin/customer")
    public Customer partialUpdate( @RequestBody String adress,@RequestParam Long id){
        Customer customertemp = customerRepo.findById(id).get();
        customertemp.setAddress(adress);
        return customerRepo.save(customertemp);
    }

}

