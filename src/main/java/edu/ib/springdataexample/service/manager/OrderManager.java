package edu.ib.springdataexample.service.manager;

import edu.ib.springdataexample.controler.Order;
import edu.ib.springdataexample.repository.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/order")
public class OrderManager {

    private OrderRepo orderRepo;

    @Autowired
    public OrderManager(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }

    @GetMapping("/all")
    public List<Order> findAll() {
        return (List<Order>) orderRepo.findAll();
    }

    @GetMapping("/one")
    public Optional<Order> findById(@RequestParam Long id){
        return orderRepo.findById(id);
    }

    @PostMapping("/admin/order")
    public Order add(@RequestBody Order order) {
        return orderRepo.save(order);
    }

    @PutMapping("/admin/order")
    public Order update(@RequestBody Order order, @RequestParam Long id) {
        orderRepo.deleteById(id);
        order.setId(id);
        return orderRepo.save(order);
    }

    @PatchMapping("/admin/order")
    public Order partialUpdate( @RequestBody String status, @RequestParam Long id){
        Order rodertemp = orderRepo.findById(id).get();
        rodertemp.setStatus(status);
        return orderRepo.save(rodertemp);
    }

}
