package edu.ib.springdataexample.service.manager;
import edu.ib.springdataexample.controler.Product;
import edu.ib.springdataexample.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/product")
public class ProductManager{


    private ProductRepo productRepo;

    @Autowired
    public ProductManager(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @GetMapping("/all")
   public List<Product> findAll() {
        return (List<Product>) productRepo.findAll();
    }
    @GetMapping("/one")
   public Optional<Product> getByID(@RequestParam Long id){
        return productRepo.findById(id);
    }

    @PostMapping("/admin/product")
    public Product postAdmin(@RequestBody Product product) {
        return productRepo.save(product);
    }

    @PutMapping("/admin/product")
    public Product postAdminByID(@RequestParam Long id, @RequestBody Product product) {
        productRepo.deleteById(id);
        product.setId(id);
        return productRepo.save(product);
    }

    @PatchMapping("/admin/product")
    public Product  patchAdminByID(@RequestParam Long id, @RequestParam float price){
        Product p = productRepo.findById(id).get();
        p.setPrice(price);
        return productRepo.save(p);
    }

}
